import { Injectable } from '@nestjs/common';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { UpdatePlaylistDto } from './dto/update-playlist.dto';
import { IUser } from 'src/user/user.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Playlist, PlaylistDocument } from './schema/playlist.shema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import aqp from 'api-query-params';

@Injectable()
export class PlaylistService {

  constructor(@InjectModel(Playlist.name) private playlistModel: SoftDeleteModel<PlaylistDocument>) { }  //  tiêm phần import Colection User ở bên Module vào để sử dụng các PT query

  async create(createPlaylistDto: CreatePlaylistDto, user: IUser) {
    const playlist = await this.playlistModel.create({
      ...createPlaylistDto,
      createdBy: {
        _id: user._id,
        email: user._id
      }
    });
    return {
      _id: playlist._id,
      createdAt: playlist.createdAt
    }
  }


  async findAll(currentPage: number, limit: number, userId:string, qs: string) {
    const { filter, sort, population } = aqp(qs) as any;
    // console.log("check page:", aqp(qs))
    delete filter.current;
    delete filter.pageSize;

    let offset = (+currentPage - 1) * (+limit);
    let defaultLimit = +limit ? +limit : 10;
    const totalItems = (await this.playlistModel.find(filter)).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    if (userId) {
      filter.userId = userId
    }

    const result = await this.playlistModel.find(filter)
      .skip(offset)
      .limit(defaultLimit)
      .sort(sort)
      .populate({
        path: 'userId',
        select: 'email'  // Chọn trường email từ userId
      })
      .populate({
        path: 'tracks',
        // select: 'title'  // Chọn trường title từ trackId
      })
      .select("-password")
      .exec();


    return {
      meta: {
        current: currentPage, //trang hiện tại
        pageSize: limit, //số lượng bản ghi đã lấy
        pages: totalPages, //tổng số trang với điều kiện query
        total: totalItems // tổng số phần tử (số bản ghi)
      },
      result //kết quả query
    }
  }


  async findOne(id: string) { 
    return await this.playlistModel.findById(id)
    .populate({
      path: 'userId',
      select: ' email'  // Chọn trường email từ userId
    })
    .populate({
      path: 'tracks',
      // select: 'title'  // Chọn trường title từ trackId
    })
    .select("-password")
    .exec();
    ;
  }


  async update(id: string, updatePlaylistDto: UpdatePlaylistDto, user: IUser) {
    return await this.playlistModel.updateOne({ _id: id },{
      ...updatePlaylistDto,
      updatedBy: {
        _id: user._id,
        email: user._id
      }
    });
  }

  async remove(id: string, updatePlaylistDto: UpdatePlaylistDto, user: IUser) {
    await this.playlistModel.updateOne({
      ...updatePlaylistDto,
      deletedBy: {
        _id: user._id,
        email: user._id
      }
    });
    return await this.playlistModel.softDelete({ _id: id })
  }
}
