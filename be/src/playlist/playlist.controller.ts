import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { PlaylistService } from './playlist.service';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { UpdatePlaylistDto } from './dto/update-playlist.dto';
import { User } from 'src/decorator/custommize';
import { IUser } from 'src/user/user.interface';

@Controller('playlist')
export class PlaylistController {
  constructor(private readonly playlistService: PlaylistService) {}

  @Post()
  create(@Body() createPlaylistDto: CreatePlaylistDto, @User() user:IUser) {
    return this.playlistService.create(createPlaylistDto,user);
  }

  @Get()
  findAll(
    @Query("current") currentPage:string,
    @Query("pageSize") limit:string,
    @Query("userId") userId:string,

    @Query() qs:string,
  ) {
    return this.playlistService.findAll(+currentPage, +limit ,userId ,qs);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.playlistService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePlaylistDto: UpdatePlaylistDto, @User() user:IUser) {
    return this.playlistService.update(id, updatePlaylistDto, user);
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Body() updatePlaylistDto: UpdatePlaylistDto, @User() user:IUser) {
    return this.playlistService.remove(id, updatePlaylistDto, user);
  }
}
