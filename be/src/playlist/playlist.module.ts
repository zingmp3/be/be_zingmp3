import { Module } from '@nestjs/common';
import { PlaylistService } from './playlist.service';
import { PlaylistController } from './playlist.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Playlist, PlaylistSchema } from './schema/playlist.shema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Playlist.name, schema: PlaylistSchema }])],  // import colection User vào để bên Service biết sự tồn tại của Colection User để truy vấn
  controllers: [PlaylistController],
  providers: [PlaylistService]
})
export class PlaylistModule {}
