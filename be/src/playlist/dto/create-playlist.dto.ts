import { IsArray, IsMongoId, IsNotEmpty } from "class-validator";
import mongoose from "mongoose";

export class CreatePlaylistDto {

    @IsNotEmpty({ message: "title không được để trống" })
    title: string;

    // @IsNotEmpty({ message: "userId không được để trống" })
    // userId: mongoose.Schema.Types.ObjectId;

    // @IsNotEmpty({ message: "tracks không được để trống" })
    // @IsMongoId({ each: true, message: "tracks phải là định dạng chuỗi" })
    // @IsArray({ message: "tracks phải là 1 mảng" })
    // tracks: mongoose.Schema.Types.ObjectId[];

}
