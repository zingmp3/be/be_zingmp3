import { IsMongoId, IsNotEmpty } from "class-validator";
import mongoose from "mongoose";

export class CreateCommentDto {
@IsNotEmpty({message: "content không được để trống"})
    content: string;

@IsNotEmpty({message: "userId không được để trống"})
@IsMongoId({message: 'userId phải là monngo Id'})
    userId:mongoose.Schema.Types.ObjectId;

@IsNotEmpty({message: "trackId không được để trống"})
@IsMongoId({message: 'trackId phải là monngo Id'})
    trackId: mongoose.Schema.Types.ObjectId;

}
