import { Injectable } from '@nestjs/common';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { IUser } from 'src/user/user.interface';
import { InjectModel } from '@nestjs/mongoose';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { Comment, CommentDocument } from './schema/comment.schema';
import aqp from 'api-query-params';

@Injectable()
export class CommentService {
  constructor(@InjectModel(Comment.name) private commentModel: SoftDeleteModel<CommentDocument>) { }  //  tiêm phần import Colection User ở bên Module vào để sử dụng các PT query


  async create(createCommentDto: CreateCommentDto, user:IUser) {
    const comment = await this.commentModel.create({
      ...createCommentDto,
      createdBy: {
        _id: user._id,
        email: user.email
      }
    })
    return {
      _id: comment._id,
      createdAt: comment.createdAt
    }
  }


  async findAll(currentPage:number, limit:number, trackId:string ,qs: string) {
    const { filter, sort, population } = aqp(qs) as any;
    delete filter.current;
    delete filter.pageSize;
    
    let offset = (+currentPage - 1) * (+limit);
    let defaultLimit = +limit ? +limit : 10;
    const totalItems = (await this.commentModel.find(filter)).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);
    

    if (trackId) {
      filter.trackId = trackId;
  }

    // console.log("check filter:", filter)
    
    const result = await this.commentModel.find(filter)
    .skip(offset)
    .limit(defaultLimit)
    .sort(sort)
    .populate({
      path: 'userId',
      select: 'email'  // Chọn trường email từ userId
  })
  .populate({
      path: 'trackId',
      select: 'title'  // Chọn trường title từ trackId
  })
    .select("-password")
    .exec();


    return {
      meta: {
      current: currentPage, //trang hiện tại
      pageSize: limit, //số lượng bản ghi đã lấy
      pages: totalPages, //tổng số trang với điều kiện query
      total: totalItems // tổng số phần tử (số bản ghi)
      },
      result //kết quả query
      }
  }



  async findOne(id: string) {
    return await this.commentModel.findById(id);
  }

  async update(id: string, updateCommentDto: UpdateCommentDto, user:IUser) {
    return await this.commentModel.updateOne({_id:id},{
      ...updateCommentDto,
      updatedBy: {
        _id: user._id,
        email: user.email
      }
    })
  }


  async remove(id: string, updateCommentDto: UpdateCommentDto, user:IUser) {
    await this.commentModel.updateOne({_id:id},{
      ...updateCommentDto,
      updatedBy: {
        _id: user._id,
        email: user.email
      }
    })
    return await this.commentModel.softDelete({_id:id})
  }
}
