import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Comment, CommentSchema } from './schema/comment.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Comment.name, schema: CommentSchema }])],  // import colection User vào để bên Service biết sự tồn tại của Colection User để truy vấn
  controllers: [CommentController],
  providers: [CommentService]
})
export class CommentModule {}
