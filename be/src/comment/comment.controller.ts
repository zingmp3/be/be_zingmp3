import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { Public, User } from 'src/decorator/custommize';
import { IUser } from 'src/user/user.interface';

@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @Post()
  create(@Body() createCommentDto: CreateCommentDto, @User() user:IUser) {
    return this.commentService.create(createCommentDto , user);
  }

  @Get()
  // @Public()
  findAll(
    @Query("current") currentPage:string,
    @Query("pageSize") limit:string,
    @Query("trackId") trackId:string,
    @Query() qs:string,
  ) {
    return this.commentService.findAll(+currentPage, +limit, trackId ,qs);
  }


  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.commentService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCommentDto: UpdateCommentDto, @User() user:IUser) {
    return this.commentService.update(id, updateCommentDto, user);
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Body() updateCommentDto: UpdateCommentDto, @User() user:IUser) {
    return this.commentService.remove(id, updateCommentDto, user);
  }
}
