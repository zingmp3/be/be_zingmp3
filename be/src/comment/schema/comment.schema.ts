
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { Track } from 'src/track/schema/track.schema';
import { User } from 'src/user/schema/user.schema';

export type CommentDocument = HydratedDocument<Comment>;

@Schema({ timestamps: true })
export class Comment {
    @Prop()
    content: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })  // ref: Company.name để dùng populate join vào bảng qua Id để lấy thông tin 
    userId:mongoose.Schema.Types.ObjectId;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: Track.name })  // ref: Company.name để dùng populate join vào bảng qua Id để lấy thông tin 
    trackId: mongoose.Schema.Types.ObjectId;

    @Prop()
    isDelete: boolean;  // thêm isDelete và deleteAt để soft delete cập nhật khi xóa

    @Prop()
    createdAt: Date;

    @Prop()
    updatedAt: Date;

    @Prop()
    deletedAt: Date;

    @Prop({ type: Object })
    createdBy: {
        _id: mongoose.Schema.Types.ObjectId,
        email: string
    };

    @Prop({ type: Object })
    updatedBy: {
        _id: mongoose.Schema.Types.ObjectId,
        email: string
    };

    @Prop({ type: Object })
    deletedBy: {
        _id: mongoose.Types.ObjectId,
        email: string
    };
}

export const CommentSchema = SchemaFactory.createForClass(Comment);