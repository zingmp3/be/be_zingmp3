import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserModule } from './user/user.module';
import { TrackModule } from './track/track.module';
import { PlaylistModule } from './playlist/playlist.module';
import { LikeModule } from './like/like.module';
import { CommentModule } from './comment/comment.module';
import { FileModule } from './file/file.module';
import { AuthModule } from './auth/auth.module';
import { softDeletePlugin } from 'soft-delete-plugin-mongoose';

@Module({
  imports: [
    // app.module là root
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGODB_URL'),  // ở module phải khai báo 1 khối code ConfigService như này để lấy biến trong file .env
        connectionFactory: (connection) => {  // phần code này là để cấu hình soft-delete global
          connection.plugin(softDeletePlugin);
          return connection;
        }
      }),
      inject: [ConfigService],
    }),

    ConfigModule.forRoot({  //  
      isGlobal: true,  //  khai báo global là cho các Module con có thể dùng được Jnject đc ConfigModule lấy đc biến trong .env 
    }),

    UserModule,
    TrackModule,
    PlaylistModule,
    LikeModule,
    CommentModule,
    FileModule,
    AuthModule,

  ],
  controllers: [AppController],
  providers: [AppService,
  ],
})
export class AppModule { }
