import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IUser } from 'src/user/user.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly configService: ConfigService
  ) {
    super({
      // Auth Guard khi muốn bảo vệ Route , bằng JWT của user

      // Trong các Route mỗi lần gửi req lên server thì khối code này sẽ tự động lấy JWT ở Header (TH khác là ở Bear Token trong Postman) để xác thực user 
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_ACCESS_TOKEN'),
    });
  }

  async validate(payload: IUser) {
    // console.log("check payload:", payload)
    return {
      _id: payload._id,
      email: payload.email,
      name: payload.name,
      role: payload.role,
    };
  }
}