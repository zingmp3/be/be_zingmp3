import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { IS_PUBLIC_KEY } from 'src/decorator/custommize';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {  //  khai báo Global
  constructor(private reflector: Reflector) {
    super();
  }

  //   khai báo Global
  //  khối code này là nó sẽ gửi Metadata (tức thông tin user) k cần phải Khai báo @UseGuards(JwtAuthGuard) ở đầu Route
  canActivate(context: ExecutionContext) {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) {
      return true;
    }
    return super.canActivate(context);
  }



  handleRequest(err, user, info) {
    // You can throw an exception based on either "info" or "err" arguments
    if (err || !user) {
      throw err || new UnauthorizedException("Token không hợp lệ or không có token ở Bear Token ở Request");
    }
    return user;
  }
}

// AuthGuard('jwt') để bảo vệ route khi user chưa đăng nhập thì k sử dụng đc Route