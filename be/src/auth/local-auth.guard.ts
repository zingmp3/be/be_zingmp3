import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()

//  khi báo AuthGuard('local') khi dùng sẽ lấy class LocalAuthGuard thế vào
export class LocalAuthGuard extends AuthGuard('local') {}   

//  AuthGuard là để kiểm tra xem "user" đã đăng nhập hay chưa . Hết