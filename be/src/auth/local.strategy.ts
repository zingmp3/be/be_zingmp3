import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
        super();
    }


    // username & password là 2 thuộc tính passport nó tự cung cấp nó tương ứng với TK và MK khi mình đăng nhập
    // GT:Khi đănh nhập nó sẽ chạy vào hàm này đầu tiên để xác thực user và nó sẽ trả về 1 biến là user, req.user (thông tin bản ghi tk)
    async validate(username: string, password: string): Promise<any> {
        const user = await this.authService.validateUser(username, password);
        if (!user) {
            throw new UnauthorizedException("Username và Password không hợp lệ!");
        }
        return user;  //  khi return user thì lúc này biến req.user sẽ có data
    }
}


