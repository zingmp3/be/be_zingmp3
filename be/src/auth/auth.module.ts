import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserService } from 'src/user/user.service';
import { UserModule } from 'src/user/user.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtStrategy } from './jwt/jwt.strategy';
import ms from 'ms';
import { AuthController } from './auth.controller';

@Module({
  // impoer UserModule và để sử dụng full luồng của UserModule
  imports: [UserModule, PassportModule,

    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_ACCESS_TOKEN'),
        signOptions: {
          expiresIn: ms(configService.get<string>('JWT_ACCESS_EXPIRE')) / 1000,
        },
      }),
      inject: [ConfigService],
    })
  ],
  
  controllers:[AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],  //  khi báo JwtStrategy để Modile biết sự tồn tại của nó
  exports: [AuthService]

})
export class AuthModule {



}


