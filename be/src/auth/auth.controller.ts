import { BadRequestException, Body, Controller, Get, Post, Req, Res, UseGuards } from '@nestjs/common';
import { Public, User } from 'src/decorator/custommize';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './local-auth.guard';
import { Request, Response } from 'express';



@Controller("auth")
export class AuthController {
  
  constructor(
    private authService: AuthService
  ) {}

  @Public()
  @UseGuards(LocalAuthGuard)  //  khi dùng UseGuards mó sẽ kiểm tra nếu chưa đăng nhập nó sẽ báo lỗi: Unauthorized
  @Post('login')
  async login(
    @Req() req,
    @Res({ passthrough: true }) response: Response
  ) {
    return this.authService.login(req.user, response);
  }

  @Public()
  @Post('/refresh')   //  khi pase Access_token đẻ gọi nó sẽ chạy vào file jwt.strategy.ts để giải mã token
  handleRefreshToken(@Body("refresh_token") refreshToken) {
    return this.authService.processNewToken(refreshToken);
  }
  
  @Public()
  @Post('social-media')
  getProfile(@Body() req) {
    return this.authService.loginSocialMedia(req);
    //   req.user lấy từ JWT decode ra
  }
  

}
