import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

import { IUser } from 'src/user/user.interface';
import { UserService } from 'src/user/user.service';
import ms from 'ms';
import { Response } from 'express';


@Injectable()
export class AuthService {

  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) { }


  //  Hàm validateUser để tìm TK bản ghi tương ứng trong DB 
  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByUsername(username);

    if (user) {
      const isValid = this.usersService.isValidPassword(pass, user.password)

      if (isValid === true) {
        return user;
      }
    }
    return null;
  }


  createRefreshToken = (payload) => {   //  khối code này tạo token
    const refresh_token = this.jwtService.sign(payload, {
      secret: this.configService.get<string>("JWT_REFRESH_TOKEN_SECRET"),
      expiresIn: ms(this.configService.get<string>("JWT_REFRESH_EXPIRED")) / 1000 // PT ms để cover thành minisecont
    })
    return refresh_token;
  }



  // jwtService.sign(payload)  là để endcode info user thành JWT
  async login(user: IUser, response: Response) {
    const { _id, name, email, role, type } = user;
    const payload = {
      sub: "token login",
      iss: "from server",
      user: {
        _id,
        name,
        email,
        type,
        role
      }
    }

    const refresh_token = this.createRefreshToken(payload);
    await this.usersService.updateUserToken(refresh_token, _id);

    return {
      access_token: this.jwtService.sign(payload),
      refresh_token,
      user: {
        _id,
        name,
        email,
        type,
        role
      }
    };
  }



  async processNewToken(refreshToken: string) {
    try {
      const info = this.jwtService.verify(refreshToken, {
        secret: this.configService.get<string>("JWT_REFRESH_TOKEN_SECRET"), 
      })  

      let user = await this.usersService.findUserToken(refreshToken); 
      console.log("check user:", user); 
      if (user) {
        const { _id, name, email, role } = user;
        const payload = {
          sub: "token refresh",
          iss: "from server",
          _id,
          name,
          email,
          role
        };
        const refresh_token = this.createRefreshToken(payload)

        // update user with refresh token in DB
        await this.usersService.updateUserToken(refresh_token, _id.toString())

        return {
          access_token: this.jwtService.sign(payload),
          refresh_token,
          user: {
            _id,
            name,
            email,
            role,
          }
        };
      }
    } catch (error) {
      console.log("check error:", error)
      throw new BadRequestException(`Refresh token không hợp lệ . Vui lòng login`)
    }
  }



  async loginSocialMedia(data) {
    const user = await this.usersService.userSocialMedia(data)
    const { _id, username, email, role, type } = user as any;
    const payload = {
      sub: "token login",
      iss: "from server",
      user: {
        _id,
        username,
        email,
        type,
        role
      }
    }

    const refresh_token = await this.createRefreshToken(payload);

    await this.usersService.updateUserToken(refresh_token, _id);

    return {
      access_token: this.jwtService.sign(payload),
      refresh_token,
      user: {
        _id,
        username,
        email,
        type,
        role
      }
    };
  }


}
