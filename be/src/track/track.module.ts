import { Module } from '@nestjs/common';
import { TrackService } from './track.service';
import { TrackController } from './track.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Track, TrackSchema } from './schema/track.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Track.name, schema: TrackSchema }])],  // import colection User vào để bên Service biết sự tồn tại của Colection User để truy vấn
  controllers: [TrackController],
  providers: [TrackService]
})
export class TrackModule {}
