import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';

export type TrackDocument = HydratedDocument<Track>;

@Schema({ timestamps: true })
export class Track {
    @Prop()
    title: string;

    @Prop()
    singer: string;

    @Prop()
    category: string;

    @Prop()
    imgUrl:string;

    @Prop()
    trackUrl: string;

    @Prop()
    upload: mongoose.Schema.Types.ObjectId;

    @Prop()
    isDelete: boolean;  // thêm isDelete và deleteAt để soft delete cập nhật khi xóa

    @Prop()
    createdAt: Date;

    @Prop()
    updatedAt: Date;

    @Prop()
    deletedAt: Date;

    @Prop({ type: Object })
    createdBy: {
        _id: mongoose.Schema.Types.ObjectId,
        email: string
    };

    @Prop({ type: Object })
    updatedBy: {
        _id: mongoose.Schema.Types.ObjectId,
        email: string
    };

    @Prop({ type: Object })
    deletedBy: {
        _id: mongoose.Types.ObjectId,
        email: string
    };
}

export const TrackSchema = SchemaFactory.createForClass(Track);