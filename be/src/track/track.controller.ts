import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { TrackService } from './track.service';
import { CreateTrackDto } from './dto/create-track.dto';
import { UpdateTrackDto } from './dto/update-track.dto';
import { User } from 'src/decorator/custommize';
import { IUser } from 'src/user/user.interface';

@Controller('track')
export class TrackController {
  constructor(private readonly trackService: TrackService) {}


  @Post()
  create(@Body() createTrackDto: CreateTrackDto , @User() user:IUser) {
    return this.trackService.create(createTrackDto, user);
  }

  @Get()
  findAll(
    @Query("current") currentPage:string,
    @Query("pageSize") limit:string,
    @Query() qs:string,
  ) {
    return this.trackService.findAll(+currentPage, +limit ,qs);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.trackService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTrackDto: UpdateTrackDto, @User() user:IUser) {
    return this.trackService.update(id, updateTrackDto, user);
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Body() updateTrackDto: UpdateTrackDto, @User() user:IUser) {
    return this.trackService.remove(id, updateTrackDto, user);
  }
}
