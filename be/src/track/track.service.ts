import { Injectable } from '@nestjs/common';
import { CreateTrackDto } from './dto/create-track.dto';
import { UpdateTrackDto } from './dto/update-track.dto';
import { IUser } from 'src/user/user.interface';
import { Track, TrackDocument } from './schema/track.schema';
import { InjectModel } from '@nestjs/mongoose';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import aqp from 'api-query-params';

@Injectable()
export class TrackService {
  constructor(@InjectModel(Track.name) private trackModel: SoftDeleteModel<TrackDocument>) { }  //  tiêm phần import Colection User ở bên Module vào để sử dụng các PT query

  async create(createTrackDto: CreateTrackDto, user: IUser) {
    const track = await this.trackModel.create({
      ...createTrackDto,
      createdBy: {
        _id: user._id,
        email: user.email
      }
    })
    return {
      _id: track._id,
      createdAt: track.createdAt
    }
  }

  async findAll(currentPage:number, limit:number ,qs: string) {
    const { filter, sort, population } = aqp(qs) as any;
    // console.log("check page:", aqp(qs))
    delete filter.current;
    delete filter.pageSize;

    let offset = (+currentPage - 1) * (+limit);
    let defaultLimit = +limit ? +limit : 10;
    const totalItems = (await this.trackModel.find(filter)).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);
    
    
    const result = await this.trackModel.find(filter)
    .skip(offset)
    .limit(defaultLimit)
    .sort(sort)
    .populate(population)
    .select("-password")
    .exec();

   
    return {
      meta: {
      current: currentPage, //trang hiện tại
      pageSize: limit, //số lượng bản ghi đã lấy
      pages: totalPages, //tổng số trang với điều kiện query
      total: totalItems // tổng số phần tử (số bản ghi)
      },
      result //kết quả query
      }
  }

  async findOne(id: string) {
    return await this.trackModel.findById(id);
  }

  async update(id: string, updateTrackDto: UpdateTrackDto, user: IUser) {
    return await this.trackModel.updateOne({ _id: id }, {
      ...updateTrackDto,
      updatedBy: {
        _id: user._id,
        email: user.email
      }
    })
  }

  async remove(id: string, updateTrackDto: UpdateTrackDto, user: IUser) {
    await this.trackModel.updateOne({ _id: id }, {
      ...updateTrackDto,
      deletedBy: {
        _id: user._id,
        email: user.email
      }
    })
    return await this.trackModel.softDelete({_id:id});
  }
}
