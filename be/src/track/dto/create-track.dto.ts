import { IsNotEmpty } from "class-validator";


export class CreateTrackDto {
@IsNotEmpty({message: "title không được để trống."})
    title: string;

@IsNotEmpty({message: "singer không được để trống."})
    singer: string;

@IsNotEmpty({message: "category không được để trống."})
    category: string;


}
