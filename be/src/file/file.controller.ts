import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile, UseFilters, HttpException, HttpStatus } from '@nestjs/common';
import { FileService } from './file.service';
import { UpdateFileDto } from './dto/update-file.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { Public } from 'src/decorator/custommize';
import path from 'path';
import fs from 'fs';

import mime from 'mime-types';

@Controller('file')
export class FileController {
  constructor(private readonly fileService: FileService) { }

  @Public()
  @Post('upload')
  // @UseFilters(new HttpExceptionFilter())

  @UseInterceptors(FileInterceptor('fileUpload'))
  uploadFile(@UploadedFile() file: Express.Multer.File) {
    console.log("check file upload", file);
    return {
      filename: file.filename // filename
    }
  }

  @Get('tracks')
  async getTracks() {
    const musicFolder = path.resolve(__dirname, '..', '..', 'public', 'upload');
    const files = fs.readdirSync(musicFolder).map(file => ({
      filename: file,
      url: `http://localhost:8000/upload/${file}`
  }));

  console.log("check files", files)
  return files;
  }

  @Get()
  findAll() {
    return this.fileService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fileService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateFileDto: UpdateFileDto) {
    return this.fileService.update(+id, updateFileDto);
  }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.fileService.remove(+id);
  // }

  @Delete('delete/:filename')
  remove(@Param('filename') filename: string) {
    try {
      return this.fileService.remove(filename);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.NOT_FOUND);
    }
  }
}

