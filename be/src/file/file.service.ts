import { Injectable } from '@nestjs/common';
import { CreateFileDto } from './dto/create-file.dto';
import { UpdateFileDto } from './dto/update-file.dto';
import path from 'path';
import fs from 'fs';

@Injectable()
export class FileService {
  create(createFileDto: CreateFileDto) {
    return 'This action adds a new file';
  }

  findAll() {
    return `This action returns all file`;
  }

  findOne(id: number) {
    return `This action returns a #${id} file`;
  }

  update(id: number, updateFileDto: UpdateFileDto) {
    return `This action updates a #${id} file`;
  }

  // remove(id: number) {
  //   return `This action removes a #${id} file`;
  // }



remove(filename: string): { message: string } {
    // Nối path của thư mục upload với filename
    const filePath = path.resolve(__dirname, '..', '..', 'public', 'upload', filename);


    console.log("File path:", filePath);
    
    // Kiểm tra xem file có tồn tại không
    if (fs.existsSync(filePath)) {
        try {
            fs.unlinkSync(filePath); // Xóa file
            return { message: 'File deleted successfully.' };
        } catch (error) {
            throw new Error(`Error deleting file: ${error.message}`);
        }
    } else {
        console.log("File not found at path:", filePath);
        throw new Error('File not found.');
    }
}
}
 