import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';
import { JwtAuthGuard } from './auth/jwt/jwt-auth.guard';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';
import cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  const configService = app.get(ConfigService);

  app.enableCors( //  cấu hình CORS  để cho phép FE có thể đọc ghi được API
    {
      "origin": true,  //  * là cho tất cả truy cập , "true" cho những doimain giống BE  VD: localhost => localhost
      "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
      "preflightContinue": false,
      credentials: true,  // credentials // là cho phép FE có quyền truy cập API
    }
  );

  app.useStaticAssets(join(__dirname, '..', 'public'));

  const reflector = app.get(Reflector);
  app.useGlobalGuards(new JwtAuthGuard(reflector));   //  JwtAuthGuard để khia báo Guard Global

  //  Pile là 1 middleware để validate và transformer ,  validate dùng Decorate trong Dto
  // transformer là trước khi gửi req đển controller mình muốn cover data thành (string,number ...) 
  app.useGlobalPipes(new ValidationPipe());

  app.use(cookieParser());

  await app.listen(configService.get('PORT'));
}
bootstrap();
