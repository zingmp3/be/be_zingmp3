import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<User>;

@Schema({ timestamps: true })
export class User {
    @Prop()
    email: string;

    @Prop()
    password: string;

    @Prop()
    name: string;

    @Prop()
    username: string;

    @Prop()
    age: number;

    @Prop()
    address: string;

    @Prop()
    type: string;

    @Prop()
    role: string;

    @Prop()
    refreshToken: string;

    @Prop()
    isDelete: boolean;  // thêm isDelete và deleteAt để soft delete cập nhật khi xóa

    @Prop()
    createdAt: Date;

    @Prop()
    updatedAt: Date;

    @Prop()
    deletedAt: Date;

    @Prop({ type: Object })
    createdBy: {
        _id: mongoose.Schema.Types.ObjectId,
        email: string
    };

    @Prop({ type: Object })
    updatedBy: {
        _id: mongoose.Schema.Types.ObjectId,
        email: string
    };

    @Prop({ type: Object })
    deletedBy: {
        _id: mongoose.Types.ObjectId,
        email: string
    };
}

export const UserSchema = SchemaFactory.createForClass(User);