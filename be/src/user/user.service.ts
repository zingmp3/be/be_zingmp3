import { BadRequestException, Injectable, RequestTimeoutException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schema/user.schema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { genSaltSync, hashSync, compareSync } from 'bcryptjs';
import aqp from 'api-query-params';
import { IUser } from './user.interface';

@Injectable()
export class UserService {

  constructor(@InjectModel(User.name) private userModel: SoftDeleteModel<UserDocument>) { }  //  tiêm phần import Colection User ở bên Module vào để sử dụng các PT query

  //  hàm này là cử lý endcode password khi đăng nhập
  getHashPassword(password: string) {
    const salt = genSaltSync(10);
    const hash = hashSync(password, salt);
    return hash;
  }


  async create(createUserDto: CreateUserDto, user: IUser) {
    const hasPassWord = this.getHashPassword(createUserDto.password)

    let email = await this.userModel.findOne({ email: createUserDto.email })
    console.log("check email:", email)
    if (email) {
      return new BadRequestException(`Email ${email} đã tồn tại!`)
    }
    let userData = await this.userModel.create({
      ...createUserDto,
      password: hasPassWord,
      createdBy: {
        _id: user._id,
        email: user.email
      }

    });
    return {
      _id: userData._id,
      createdAt: userData.createdAt
    }
  }

  async findAll(currentPage: number, limit: number, qs: string) {
    const { filter, sort, population } = aqp(qs) as any;
    // console.log("check page:", aqp(qs))
    delete filter.current;
    delete filter.pageSize;

    let offset = (+currentPage - 1) * (+limit);
    let defaultLimit = +limit ? +limit : 10;
    const totalItems = (await this.userModel.find(filter)).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const result = await this.userModel.find(filter)
      .skip(offset)
      .limit(defaultLimit)
      .sort(sort)
      .populate(population)
      .select("-password")
      .exec();

    return {
      meta: {
        current: currentPage, //trang hiện tại
        pageSize: limit, //số lượng bản ghi đã lấy
        pages: totalPages, //tổng số trang với điều kiện query
        total: totalItems // tổng số phần tử (số bản ghi)
      },
      result //kết quả query
    }
  }
  

  async findOne(id: string) {
    return await this.userModel.findById(id);
  }

  async findOneByUsername(username: string) {
    return await this.userModel.findOne({
      email: username
    })
  }

  isValidPassword(password: string, hash: string) {
    return compareSync(password, hash)   // compareSync để só sánh password
  }





  async update(id: string, updateUserDto: UpdateUserDto, user: IUser) {
    let email = await this.userModel.findOne({ email: updateUserDto.email })
    console.log("check email:", email)
    if (email) {
      return new BadRequestException(`Email ${email} đã tồn tại!`)
    }
    return await this.userModel.updateOne({ _id: id }, {
      ...updateUserDto,
      updatedBy: {
        _id: user._id,
        email: user.email
      }
    });
  }


  async remove(id: string, user: IUser) {
    const userId = await this.userModel.findOne({ _id: id }, {
      deletedBy: {
        _id: user._id,
        email: user.email
      }
    });
    return await this.userModel.softDelete({ _id: userId._id });
  }




  // refresh token 
  async userSocialMedia(data: IUser) {
    const user = await this.userModel.findOne({ username: data.username, type: data.type })
    if (!user) {
      return await this.userModel.create({
        username: data.username,
        email: data.username,
        password: "12345",
        type: data.type,
        role: "USER"
      })
    }
    return user;
  }


  async updateUserToken(refreshToken: string, _id:string){
    console.log("check userTokenLogin:", refreshToken)
    return await this.userModel.updateOne({_id},{
      refreshToken: refreshToken
    })
  }

  async findUserToken(refreshToken: string){
    console.log("check userTokenRefresh:", refreshToken)
    const user =  await this.userModel.findOne({refreshToken})
    return user
  }


}
