
import { IsEmail, IsNotEmpty } from 'class-validator';


export class CreateUserDto {
    @IsNotEmpty({ message: "email is not empty" })
    @IsEmail({}, { message: "Field must be email" })
    email: string;

    // @IsNotEmpty({ message: "password is not empty" })
    password: string;

    @IsNotEmpty({ message: "name is not empty" })
    name: string;

    @IsNotEmpty({ message: "age is not empty" })
    age: number;

    @IsNotEmpty({ message: "address is not empty" })
    address: string;

    @IsNotEmpty({ message: "type is not empty" })
    type: string;

    @IsNotEmpty({ message: "role is not empty" })
    role: string;
   
}
