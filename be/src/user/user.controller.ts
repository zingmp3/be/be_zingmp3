import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { IUser } from './user.interface';
import { User } from 'src/decorator/custommize';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto, @User() user:IUser) {
    return this.userService.create(createUserDto, user);
  }

  @Get()
  findAll(
    @Query("current") currentPage:string,
    @Query("pageSize") limit:string,
    @Query() qs:string,
  ) {
    return this.userService.findAll(+currentPage, +limit ,qs);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto, @User() user:IUser) {
    return this.userService.update(id, updateUserDto, user);
  }

  @Delete(':id')
  remove(@Param('id') id: string ,@User() user:IUser) {
    return this.userService.remove(id , user);
  }
}

