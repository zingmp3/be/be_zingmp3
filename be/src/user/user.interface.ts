export interface IUser {
    _id: string;
    name: string;
    username: string;
    email: string;
    type:string,
    role: string;
    }