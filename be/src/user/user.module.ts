import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './schema/user.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: User.name, schema: UserSchema }])],  // import colection User vào để bên Service biết sự tồn tại của Colection User để truy vấn
  controllers: [UserController],
  providers: [UserService],
  exports:[UserService]
})
export class UserModule {}

