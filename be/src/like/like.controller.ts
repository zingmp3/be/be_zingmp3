import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { LikeService } from './like.service';
import { CreateLikeDto } from './dto/create-like.dto';
import { UpdateLikeDto } from './dto/update-like.dto';
import { User } from 'src/decorator/custommize';
import { IUser } from 'src/user/user.interface';

@Controller('like')
export class LikeController {
  constructor(private readonly likeService: LikeService) {}

  @Post()
  create(@Body() createLikeDto: CreateLikeDto, @User() user:IUser) {
    return this.likeService.create(createLikeDto,user);
  }

  @Get()
  findAll(
    @Query("current") currentPage:string,
    @Query("pageSize") limit:string,
    @Query("userId") userId:string,
    
    @Query() qs:string,
  ) {
    return this.likeService.findAll(+currentPage, +limit, userId ,qs);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.likeService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLikeDto: UpdateLikeDto, @User() user:IUser) {
    return this.likeService.update(id, updateLikeDto, user);
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Body() updateLikeDto: UpdateLikeDto, @User() user:IUser) {
    return this.likeService.remove(id, updateLikeDto, user);
  }
}
