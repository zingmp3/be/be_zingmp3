import { IsArray, IsMongoId, IsNotEmpty } from "class-validator";
import mongoose from "mongoose";


export class CreateLikeDto {

    @IsNotEmpty({message:"userId không được để trống"})
    userId: mongoose.Schema.Types.ObjectId;
 
    // @IsNotEmpty({message:"tracks không được để trống"})
    @IsMongoId({ each: true, message:"tracks phải là định dạng chuỗi"})
    tracks: mongoose.Schema.Types.ObjectId[];

}
