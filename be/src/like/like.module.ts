import { Module } from '@nestjs/common';
import { LikeService } from './like.service';
import { LikeController } from './like.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Like, LikeSchema } from './schema/like.shema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Like.name, schema: LikeSchema }])],  // import colection User vào để bên Service biết sự tồn tại của Colection User để truy vấn
  controllers: [LikeController],
  providers: [LikeService]
})
export class LikeModule {}
