
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { Track } from 'src/track/schema/track.schema';
import { User } from 'src/user/schema/user.schema';

export type LikeDocument = HydratedDocument<Like>;

@Schema({ timestamps: true })
export class Like {

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
    userId: mongoose.Schema.Types.ObjectId;
    
    @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: Track.name })
    tracks: Track[];

    @Prop()
    isDelete: boolean;  // thêm isDelete và deleteAt để soft delete cập nhật khi xóa

    @Prop()
    createdAt: Date;

    @Prop()
    updatedAt: Date;

    @Prop()
    deletedAt: Date;

    @Prop({ type: Object })
    createdBy: {
        _id: mongoose.Schema.Types.ObjectId,
        email: string
    };

    @Prop({ type: Object })
    updatedBy: {
        _id: mongoose.Schema.Types.ObjectId,
        email: string
    };
    
    @Prop({ type: Object })
    deletedBy: {
        _id: mongoose.Types.ObjectId,
        email: string
    };
}

export const LikeSchema = SchemaFactory.createForClass(Like);