import { Injectable } from '@nestjs/common';
import { CreateLikeDto } from './dto/create-like.dto';
import { UpdateLikeDto } from './dto/update-like.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Like, LikeDocument } from './schema/like.shema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { IUser } from 'src/user/user.interface';
import aqp from 'api-query-params';

@Injectable()
export class LikeService {

  constructor(@InjectModel(Like.name) private likeModel: SoftDeleteModel<LikeDocument>) { }  //  tiêm phần import Colection User ở bên Module vào để sử dụng các PT query


  async create(createLikeDto: CreateLikeDto, user: IUser) {
    console.log("check createLikeDto", createLikeDto)
    const like = await this.likeModel.create({
      ...createLikeDto,
      createdBy: {
        _id: user._id,
        email: user.email
      }
    })
    return {
      _id: like._id,
      createdAt: like.createdAt
    }
  }

  async findAll(currentPage: number, limit: number, userId:string, qs: string) {
    const { filter, sort, population } = aqp(qs) as any;
    // console.log("check page:", aqp(qs))
    delete filter.current;
    delete filter.pageSize;

    let offset = (+currentPage - 1) * (+limit);
    let defaultLimit = +limit ? +limit : 10;
    const totalItems = (await this.likeModel.find(filter)).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);
    
    if (userId) {
      filter.userId = userId;
    }

    const result = await this.likeModel.find(filter)
      .skip(offset)
      .limit(defaultLimit)
      .sort(sort)
      .populate({
        path: 'userId',
        select: '_id email',  // Chọn trường email từ userId
      })
      .populate({
        path: 'tracks',
        // select: 'title'  // Chọn trường title từ trackId
      })
      .select("-password")
      .exec();


    return {
      meta: {
        current: currentPage, //trang hiện tại
        pageSize: limit, //số lượng bản ghi đã lấy
        pages: totalPages, //tổng số trang với điều kiện query
        total: totalItems // tổng số phần tử (số bản ghi)
      },
      result //kết quả query
    }
  }

  async findOne(id: string) {
    return await this.likeModel.findById(id);
  }

  async update(id: string, updateLikeDto: UpdateLikeDto, user: IUser) {
    console.log("check id like", id)
    return await this.likeModel.updateOne({ _id: id }, {
      ...updateLikeDto,
      updatedBy: {
        _id: user._id,
        email: user.email
      }
    })
  }

  async remove(id: string, updateLikeDto: UpdateLikeDto, user: IUser) {
    await this.likeModel.updateOne({ _id: id }, {
      ...updateLikeDto,
      deletedBy: {
        _id: user._id,
        email: user.email
      }
    })

    return await this.likeModel.softDelete({ _id: id });

  }

}
